// MongoDB Operations
// For creating or inserting data into database
db.users.insert({
    firstName: "Ely",
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "534776",
        email: "ely@eraserheads.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
})

// For querying all the data in database
// For finding documents
db.users.find();

// For creating or inserting many data into database
db.users.insertMany([
	{
		firstName: "Chito",
		lastName: "Miranda",
		age: 43,
		contact: {
			phone: "5347786",
			email: "chito@parokya.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Francis",
		lastName: "Magalona",
		age: 61,
		contact: {
			phone: "5347786",
			email: "francisM@email.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])

// ================================================

// for querying all the data in database
// for finding documents

// FIND ALL
db.users.find();

// FIND [One]
db.users.find({firstName: "Francis", age: 61});

// ================================================

// To insert in a database
db.users.insert(
	{
		firstName: "Gloc-9",
		lastName: "Walang Apelyido",
		age: 43,
		contact: {
			phone: "5336234",
			email: "gloc@parokya.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	}

);

// To delete in a database
db.users.deleteOne({
    firstName: "Ely"
});

// to update a data in a database
db.users.updateOne(
    {
        firstName: "Gloc-9"
    },
    {
        $set: {
            department: "DOH"
        }
    }
);

// to delete many
db.users.deleteMany({
    department: "DOH"
});

// to update many
db.users.updateMany(
    {
        department: "none"
    },
    {
        $set: {
            department: "HR"
        }
    }
);